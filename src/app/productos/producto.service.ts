import { Injectable } from '@angular/core';

export class Producto{
  id : number;
  codigo : string;
  nombre : string;
  precio : number;
  constructor(id: number, codigo:string, nombre:string,precio:number){
    this.id = id;
    this.codigo = codigo;
    this.nombre = nombre;
    this.precio = precio;
  }
}
@Injectable()
export class ProductoService {
  constructor() { }
  public getAll() : Producto[] {
    return [
      new Producto(1,'CFANDTTI','Cafe Andati Grande',14.5),
      new Producto(2,'GTASAMAL','Galletas de Animalito',30.00),
      new Producto(3,'PAPS280G','Papas fritas de 280grm',50.00)
    ];

  }

}
