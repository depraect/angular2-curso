import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
// Propiedades
  public title: string;
  public name: string;
  public btnTitle: string;

  public show: boolean;

  public items: string[];

  public estilo: any;


  public color: string;


  public itemSeleccionado: number;
//construnctor
  constructor() {
    this.name = 'hola';
    this.title = 'angular 2';
    this.show = true;
    this.btnTitle = 'Ocultar';
    this.items = [];
    this.items.push('Item 1');
    this.items.push('Item 2');
    this.items.push('Item 3');
    this.items.push('Item 4');

    this.itemSeleccionado = 0;

    this.color = 'red';

    this.estilo = {'color': 'blue' };
    this.estilo.color = 'red';
  }

//Metodos
  onShow() {
    this.show = !this.show;
    if (this.show) {
      this.btnTitle = 'Ocultar';
    } else {
      this.btnTitle = 'Mostrar';
    }
  }


  agregaItems() {
    this.items.push('Item ' + (this.items.length + 1));
  }


  quitarItem(index: number) {
    this.items.splice(index, 1);
  }


  seleccionarItem(index: number) {
    this.itemSeleccionado = index;
  }

  cssPar(index: number): boolean {
    return index%2 === 0;
  }

  ponerEstilo(index: number): any {
    return {'par': this.cssPar(index), 'impar': !this.cssPar(index) };
  }

  setStyle(index: number): string {
    return ((index + 1) * 5) + 'pt';
  }


}
