import { CursoformsPage } from './app.po';

describe('cursoforms App', function() {
  let page: CursoformsPage;

  beforeEach(() => {
    page = new CursoformsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
